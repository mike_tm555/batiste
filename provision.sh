#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
sudo apt-get -q -y  update
sudo apt-get -q -y  upgrade
sudo apt-get install -q -y  vim aptitude curl wget telnet
sudo locale-gen "en_US.UTF-8"
# Install MySQL
mkdir -p /var/lib/mysql
echo mysql-server-5.1 mysql-server/root_password password 123456 | sudo debconf-set-selections
echo mysql-server-5.1 mysql-server/root_password_again password 123456 | sudo debconf-set-selections
sudo apt-get install -q -y mysql-server
sudo service mysql restart;
mysql -uroot -p123456 -e "SET PASSWORD = PASSWORD('');"
sudo mysql -u root -e 'CREATE DATABASE batiste;'
# Install Apache2
sudo apt-get install -q -y apache2
# Install Apache2 mod rewrite
sudo a2enmod rewrite
# Install PHP
sudo apt-get install -q -y php5 libapache2-mod-php5
# Install PHP Extensions
sudo apt-get install -q -y php5-intl php5-gd php5-curl php5-mysql php-pear php5-imagick php5-imap php5-mcrypt php5-memcached php5-memcache php5-dev libpcre3-dev  php5-xdebug
# Reload apache configuration
sudo rm -rf /var/www
sudo ln -fs /docker /var/www

sudo /etc/init.d/apache2 restart
sudo /etc/init.d/apache2 stop

sudo rm /etc/apache2/sites-available/000-default.conf;
sudo touch /etc/apache2/sites-available/000-default.conf;
sudo bash -c 'cat >> /etc/apache2/sites-available/000-default.conf' <<'EOF'
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF


# Install git memcached
sudo aptitude install -q -y nodejs memcached git npm nodejs-legacy
sudo bash -c "echo \"innodb_buffer_pool_size = 2M\" >> /etc/mysql/my.cnf"

sudo npm install webpack -g
# Install wp-cli
sudo wget -O /usr/local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x /usr/local/bin/wp

mkdir /home/docker/.wp-cli
wget -O /home/docker/.wp-cli/wp-completion.bash https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash 
chmod +x /home/docker/.wp-cli/wp-completion.bash
echo "source /home/docker/.wp-cli/wp-completion.bash" >> /home/docker/.bashrc


echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | sudo debconf-set-selections
echo 'phpmyadmin phpmyadmin/app-password-confirm password ' | sudo debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/admin-pass password ' | sudo debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/app-pass password ' | sudo debconf-set-selections
echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | sudo debconf-set-selections
sudo apt-get install -q -y  phpmyadmin
sudo sudo service apache2 stop

sudo rm /etc/apache2/apache2.conf
sudo touch /etc/apache2/apache2.conf
sudo bash -c 'cat >> /etc/apache2/apache2.conf' <<'EOF'
ServerName localhost
Mutex file:${APACHE_LOCK_DIR} default
PidFile ${APACHE_PID_FILE}
Timeout 300
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 5
User docker
Group docker
HostnameLookups Off
ErrorLog ${APACHE_LOG_DIR}/error.log
LogLevel warn
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
Include ports.conf
<Directory />
        Options FollowSymLinks
        AllowOverride None
        Require all denied
</Directory>
<Directory /usr/share>
        AllowOverride None
        Require all granted
</Directory>
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
AccessFileName .htaccess
<FilesMatch "^\.ht">
        Require all denied
</FilesMatch>
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
IncludeOptional conf-enabled/*.conf
IncludeOptional sites-enabled/*.conf
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

sudo service apache2 restart

cd /docker
sudo rm /etc/mysql/debian.cnf
sudo touch /etc/mysql/debian.cnf
sudo bash -c 'cat >> /etc/mysql/debian.cnf' <<'EOF'
[client]
host     = localhost
user     = root
password = ''
socket   = /var/run/mysqld/mysqld.sock
[mysql_upgrade]
host     = localhost
user     = root
password = ''
socket   = /var/run/mysqld/mysqld.sock
basedir  = /usr
EOF


# Ruby Dependencies
sudo apt-get  -q -y -f install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev
sudo apt-get -q -y -f remove ruby

# Install rbenv
cd /home/docker
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> /home/docker/.bashrc
echo 'eval "$(rbenv init -)"' >> /home/docker/.bashrc
#exec $SHELL

# Install ruby-build
git clone git://github.com/sstephenson/ruby-build.git /home/docker/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> /home/docker/.bashrc
#exec $SHELL

git clone https://github.com/sstephenson/rbenv-gem-rehash.git /home/docker/.rbenv/plugins/rbenv-gem-rehash

/home/docker/.rbenv/bin/rbenv install 2.2.2
/home/docker/.rbenv/bin/rbenv rehash
/home/docker/.rbenv/bin/rbenv global 2.2.2


# No local package documentation && bundler install
echo "gem: --no-ri --no-rdoc" > ~/.gemrc
sudo /home/docker/.rbenv/shims/gem install bundler
# Install gems
sudo /home/docker/.rbenv/shims/gem install capistrano
sudo /home/docker/.rbenv/shims/gem install capistrano-slackify



# Allow phpmyadmin no password login
sudo bash -c "echo \"\\\$cfg['Servers'][1]['AllowNoPassword'] = TRUE;\" >> /etc/phpmyadmin/config.inc.php"

# Change ssh keys location.
mkdir /home/docker/.ssh
touch /home/docker/.ssh/config
touch /home/docker/.ssh/known_hosts
touch /docker/ssh_keys/known_hosts
sudo chown -v docker /home/docker/.ssh/known_hosts
echo "IdentityFile /docker/ssh_keys/id_rsa" >>  /home/docker/.ssh/config
echo "UserKnownHostsFile /docker/ssh_keys/known_hosts" >>  /home/docker/.ssh/config

# Global git config
USERNAME=$1
USEREMAIL=$2
# Global git config
cat >> /home/docker/.gitconfig <<EOF
[user]
        name = $USERNAME
        email = $USEREMAIL
[core]
	autocrlf = false
	eol = lf
EOF

# .gitattributes
cat >> /home/docker/.gitattributes << 'EOF'
* text=auto
EOF

cd /docker
sudo npm install


echo "cd /docker" >> /home/docker/.bashrc
echo "zsh" >> /home/docker/.bashrc

sudo apt-get install -q -y -f zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> /home/docker/.zshrc
echo 'eval "$(rbenv init -)"' >> /home/docker/.zshrc
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> /home/docker/.zshrc
touch /docker/docker/.zsh_history
echo 'HISTFILE=/docker/docker/.zsh_history' >> /home/docker/.zshrc
sudo chown docker:docker /docker/ssh_keys/id_rsa
sudo chown docker:docker /docker/ssh_keys/id_rsa.pub
sudo chmod 600 /docker/ssh_keys/id_rsa
cd wordpress
sudo rm .git
touch .git
cat >> .git << 'EOF'
gitdir: ../.git/modules/wordpress/
EOF
sudo chmod 770 /docker/content/themes/titus/templates_c

sudo apt-get install openssh-server -q -y -f


sudo -u root bash << EOF
    echo -e "docker\ndocker" | passwd docker
EOF

sudo  chmod +x /docker/start.sh
sudo cp /docker/batiste /usr/local/bin/batiste

exit


