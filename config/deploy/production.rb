############################################
# Setup Server
############################################
set :stage, :production
set :stage_url, "http://test-karlen1993.rhcloud.com"
server "test-karlen1993.rhcloud.com", user: "573b64a889f5cfb9f10000f3", roles: %w{web app db}
set :deploy_to, "/var/lib/openshift/573b64a889f5cfb9f10000f3/app-root/runtime/repo/www"

############################################
# Setup Git
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

#set :ssh_options, {
#    auth_methods: %w(password),
#    password: 'password',
#    user: 'username',
#}

#specify a specific temp dir if user is jailed to home
#set :tmp_dir, "/path/to/custom/tmp"
