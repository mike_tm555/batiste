'use strict';

var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var modules_folder = path.resolve('./')+"/content/themes/batiste/modules";

function Plugin(){

  function generateMainJs(callback){
    var js_modules = getDirectories(modules_folder, 'js');
    var main_js = path.resolve('./')+"/content/themes/batiste/assets/js/main.js";
    copy_to_tmp(main_js, function(){
      var imports = "";
      js_modules.map(function(item, index) {
        imports += "import 'modules/"+item+"/index.js';\r\n";
      });
      fs.appendFile(main_js, imports, function (err) {
        console.log("\r\n************************************************\r\n");
        console.log('added '+js_modules.length+' js modules');
        console.log("\r\n************************************************\r\n");
        console.log(js_modules);
        console.log("\r\n************************************************\r\n");
        callback();
      });
    });

  }

  function generateMainScss(callback){
    var scss_modules = getDirectories(modules_folder, 'scss');
    var main_scss = path.resolve('./')+"/content/themes/batiste/assets/scss/main.scss";
    copy_to_tmp(main_scss, function(){
      var imports = "";
      scss_modules.map(function(item, index) {
        imports += "@import '../../modules/"+item+"/index';\r\n";
      });
      fs.appendFile(main_scss, imports, function (err) {
        console.log('added '+scss_modules.length+' scss modules');
        console.log("\r\n************************************************\r\n");
        console.log(scss_modules);
        console.log("\r\n************************************************\r\n");
        callback();
      });
    });


  }

  function getDirectories(srcpath, type) {
    return fs.readdirSync(srcpath).filter(function(file) {
      return (fs.statSync(path.join(srcpath, file)).isDirectory() && fileExists(path.join(srcpath, file)+'/index.'+type));
    });
  }

  function copy_to_tmp(file, callback){
    var rs = fs.createReadStream(file);
    var ws = fs.createWriteStream(file+'.tmp');
    rs.pipe(ws);
    ws.on( 'close', callback);
  }

  function fileExists(filePath){
    try
    {
      return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
      return false;
    }
  }

  this.apply = function(compiler) {

    compiler.plugin("make",function(compilation, webpack_make_callback){
      generateMainJs(function(){
        generateMainScss(function(){
          webpack_make_callback();
        });
      });
    });

    compiler.plugin("done",function(compilation, webpack_done_callback){
      var main_scss = path.resolve('./')+"/content/themes/batiste/assets/scss/main.scss";
      var main_js = path.resolve('./')+"/content/themes/batiste/assets/js/main.js";

      fs.unlink(main_scss, function(){

      });

      fs.unlink(main_js, function(){

      });
      fs.rename(main_scss+'.tmp', main_scss);
      fs.rename(main_js+'.tmp', main_js);
    });
  }

};

module.exports = Plugin;
