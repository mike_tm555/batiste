<?php
spl_autoload_register(function ($class) {
    $isClass = explode('\\', $class);
    if($isClass[0] == "Batiste") {
        $path = strtolower( $class );
        $path = str_replace( '_', '-',  $path );
        $path = explode( '\\', $path );
        $file = array_pop( $path );
        $path = implode( '/', $path ) . '/' . $file . '.php';
        $plugins_dir = WP_PLUGIN_DIR.'/';
        $path = $plugins_dir . $path;
        if ( file_exists( $path ) ) {
            require_once $path;
        }
    } if($isClass[0] == "Modules") {
        $path = strtolower( $class );
        $path = str_replace( '_', '-',  $path );
        $path = explode( '\\', $path );
        $file = array_pop( $path );
        $path = implode( '/', $path ) . '/' . $file . '/index.php';
        $theme_dir = get_template_directory().'/';
        $path = $theme_dir . $path;
        if ( file_exists( $path ) ) {
            require_once $path;
        }
    }
});
