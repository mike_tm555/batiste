<?php
namespace Batiste\Core;

class Module {

    protected $data = array(

    );

    protected $args = array(

    );

    protected $moduleName;

    public function __construct(){
        $className = get_called_class();
        $className = explode('\\', $className);
        $module = strtolower(array_pop($className));
        $module = str_replace('_', '-', $module);
        $this->moduleName = $module;
    }

    protected function setData(){
        $this->data = array();
    }

    public function init($args = false) {
        $this->args = ($args != false) ? $args : $this->args;
        $this->setData();
        $this->render();
    }

    public function init_get($args = false) {
        $this->args = ($args != false) ? $args : $this->args;
        $this->setData();
        return $this->render_get();
    }

    protected function render() {
        $theme_dir = get_template_directory().'/';
        $template_path = $theme_dir.'modules/'.$this->moduleName.'/index.tpl';
        $smarty = smarty_get_instance();
        foreach($this->data as $key => $value){
            $smarty->assign($key, $value);
        }
        $smarty->display($template_path);
    }

    protected function render_get() {
        $theme_dir = get_template_directory().'/';
        $template_path = $theme_dir.'modules/'.$this->moduleName.'/index.tpl';
        $smarty = smarty_get_instance();
        foreach($this->data as $key => $value){
            $smarty->assign($key, $value);
        }
        return $smarty->fetch($template_path);
    }
}