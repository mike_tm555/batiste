<?php
/**
 * @package WordPress
 * @subpackage Batiste
 */
?>
<!DOCTYPE html>
<html id="ie7" <?php language_attributes(); ?>>
<html id="ie8" <?php language_attributes(); ?>>
<html id="ie9" <?php language_attributes(); ?>>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Es-Salon">
    <meta property="og:url" content="<?php echo( home_url( '/' ) ); ?>">
    <meta property="og:image" content="<?php ?>">
    <meta property="og:description" content="<?php ?>">
    <title><?php echo bloginfo('name'); ?></title>
    <meta name="description" content="">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/build/app.min.css" />
    <?php wp_head(); ?>
</head>
<body>