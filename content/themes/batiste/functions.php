<?php

require_once dirname( __FILE__ ) . '/ajax.php';
add_action('init', 'batiste_init', 1);
add_action('init', 'create_postType');

function create_postType() {

    register_post_type( 'products',
        array(
            'labels' => array(
                'name' => __( 'Продукты' ),
                'singular_name' => __( 'Продукты' )
            ),
            'public'       => true,
            'menu_icon'    => 'dashicons-products',
            'has_archive'  => true,
            'show_ui'      => true,
            'hierarchical' => false,
            'taxonomies'   => array('category'),
            'supports'     => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
        )
    );

    register_post_type( 'brands',
        array(
            'labels' => array(
                'name' => __( 'Бренды' ),
                'singular_name' => __( 'Бренды' )
            ),
            'public'      => true,
            'menu_icon'   => 'dashicons-shield-alt',
            'has_archive' => true,
            'supports'    => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
        )
    );

    register_taxonomy_for_object_type( 'category', 'brands' );
    register_taxonomy_for_object_type( 'post_tag', 'brands' );

    register_post_type( 'cards',
        array(
            'labels' => array(
                'name' => __( 'Открытки' ),
                'singular_name' => __( 'Открытки' )
            ),
            'public'      => true,
            'menu_icon'   => 'dashicons-book',
            'has_archive' => true,
            'supports'    => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
        )
    );

    register_taxonomy_for_object_type( 'category', 'cards' );
    register_taxonomy_for_object_type( 'post_tag', 'cards' );

    register_post_type( 'slider',
        array(
            'labels' => array(
                'name' => __( 'Слайдер' ),
                'singular_name' => __( 'Слайдер' )
            ),
            'public'      => true,
            'menu_icon'   => 'dashicons-slides',
            'has_archive' => true,
            'supports'    => array('title', 'author', 'thumbnail', 'excerpt')
        )
    );
}


function batiste_init()
{
    add_filter( 'show_admin_bar', '__return_false' );

    add_image_size( '600x600', 600, 600, true );
    add_image_size( '600x400', 600, 400, true );

    add_theme_support('post-thumbnails');

    register_taxonomy(
        'spellings',
        array(
            'products',
        ),
        array(
            'label' => 'Ароматы',
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'notes',
        array(
            'products',
        ),
        array(
            'label' => 'Ноты',
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'volume',
        array(
            'products',
        ),
        array(
            'label' => 'Объем',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'concentration',
        array(
            'products',
        ),
        array(
            'label' => 'Концентрация',
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'slider',
        array(
            'slider',
        ),
        array(
            'label' => 'Тип Слайдера',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'popularity',
        array(
            'products',
        ),
        array(
            'label' => 'Популярность',
            'meta_box_cb' => 'post_categories_meta_box',
            'hierarchical' => true
        )
    );

    register_taxonomy(
        'catalog',
        array(
            'products',
        ),
        array(
            'label' => 'Каталог',
            'meta_box_cb' => 'post_categories_meta_box',
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => true
        )
    );

    register_taxonomy(
        'category',
        array(
            'products',
        ),
        array(
            'label' => 'Категории',
            'meta_box_cb' => 'post_categories_meta_box',
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'category',
                'hierarchical' => true
            )
        )
    );
}

function batiste_category() {
    $category       = get_queried_object();
    $category->link = get_term_link($category->cat_ID);
    return $category;
}

function batiste_catalog() {
    $term    = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    $catalog = Array();

    $arguments = array(
        'type'         => 'post',
        'child_of'     => 0,
        'parent'       => '',
        'orderby'      => 'name',
        'order'        => 'ASC',
        'hide_empty'   => false,
        'hierarchical' => 1,
        'number'       => '',
        'taxonomy'     => 'catalog',
        'pad_counts'   => false
    );

    $catalogData  = get_categories($arguments);
    $categoryData = batiste_category();

    foreach($catalogData as $key => $value) {
        if($value->parent == 0) {
            $value->link = $categoryData->link.$value->slug."/";
            $catalog['parent'][$value->cat_ID] = $value;
        } else {
            $parent = get_term($value->parent);
            $value->link = $categoryData->link.$parent->slug."/".$value->slug."/";
            $catalog["childes"][$value->parent][] = $value;
        }
    }

    if(!empty($sector)) {
        $catalog["active"] = $term;
    } else {
        $catalog["active"] = false;
    }

    return $catalog;
}

function new_subcategory_hierarchy() {
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php';
    }
    return locate_template( $templates );
}

add_filter('category_template', 'new_subcategory_hierarchy');

add_theme_support('menus');
add_theme_support('widgets');
add_theme_support('title-tag');
add_theme_support('custom-logo');
add_theme_support('custom-header');
add_theme_support('custom-background');
add_theme_support('post-thumbnails');
add_theme_support('post-formats',
    array(
        'aside',
        'gallery'
    )
);

add_theme_support('html5',
    array(
        'comment-list',
        'comment-form',
        'search-form',
        'gallery',
        'caption'
    )
);