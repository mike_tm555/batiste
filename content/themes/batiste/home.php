<?php
/**
 * Created by Karlen Avetisyan.
 * User: karlen
 * Date: 6/18/16
 * Time: 10:55 PM
 */


ini_set('display_errors', 1);
error_reporting(E_ALL);

get_header();
load_module('Header');
load_module('Napper');
load_module('Menu');
load_module('Slider');
load_module('Popular');
load_module('Recommend');
load_module('Subscribe');
load_module('Footer');
get_footer();
