<div class="module-filter">
    <div class="app-rel app-box filters-container">

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Цена</span>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-from"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text">от:</span>
                </label>
                <input id="price-filter-from"
                       class="app-rel app-box price-input"
                       type="number"
                       min="{$filters.min}"
                       max="{$filters.max}"
                       step="50"
                       value="{$filters.min}"/>
                <div class="price-label-key">{$filters.priceKey}</div>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-to"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text">до:</span>
                </label>
                <input id="price-filter-to"
                       class="app-rel app-box price-input"
                       type="number"
                       min="{$filters.min}"
                       max="{$filters.max}"
                       step="50"
                       value="{$filters.max}"/>
                <div class="price-label-key">{$filters.priceKey}</div>
            </div>
        </div>

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Бренды</span>
            </div>
            <select id="select-brands"
                    name="brand"
                    title="Найти бренд"
                    multiple="multiple">
                {foreach from=$filters.brands item=brand}
                    <option value="{$brand->ID}">
                        {$brand->post_title}
                    </option>
                {/foreach}
            </select>
        </div>

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Օбъем</span>
            </div>
            <select id="select-volumes"
                    name="volume"
                    title="Найти объем"
                    multiple="multiple">
                {foreach from=$filters.volumes item=volume}
                    <option value="{$volume->term_id}">
                        {$volume->name} {$filters.volumeKey}
                    </option>
                {/foreach}
            </select>
        </div>


        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Ноты</span>
            </div>
            <select id="select-notes"
                    name="notes"
                    title="Найти ноты"
                    multiple="multiple">
                {foreach from=$filters.notes item=note}
                    <option value="{$note->term_id}">
                        {$note->name}
                    </option>
                {/foreach}
            </select>
        </div>

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Ароматы</span>
            </div>
            <select id="select-spellings"
                    name="spellings"
                    title="Найти аромат"
                    multiple="multiple">
                {foreach from=$filters.spellings item=spelling}
                    <option value="{$spelling->term_id}">
                        {$spelling->name}
                    </option>
                {/foreach}
            </select>
        </div>

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Концентрации</span>
            </div>
            <select id="select-consists"
                    name="consists"
                    title="Найти концентрацию"
                    multiple="multiple">
                {foreach from=$filters.consists item=consist}
                    <option value="{$consist->term_id}">
                        {$consist->name}
                    </option>
                {/foreach}
            </select>
        </div>

        <div class="app-rel app-box filter-selected-items"
             id="filter-selected-items">
        </div>
    </div>
</div>