<?php

namespace Modules;


class Filter extends \Batiste\Core\Module
{
    protected $publishedIds = false;

    protected function getPublishedPostIds() {
        $postIds = get_posts(array(
            'post_type'   => 'perfume',
            'showposts'   => -1,
            'post_status' => 'publish',
            'fields'      => 'ids',
        ));

        $postIds = implode(',', $postIds);

        return $postIds;
    }

    protected function getPriceRange() {
        $prices = array(
            "min" => 500,
            "max" => 100000
        );

        global $wpdb;

        $query = "SELECT
                max(cast(meta_value as unsigned)) AS max,
                min(cast(meta_value as unsigned)) AS min
                FROM wp_postmeta
                WHERE meta_key='price'
                AND post_id IN ({$this->publishedIds})";

        $range = $wpdb->get_results($query);

        if(!empty($range) &&
            !empty($range[0]->min) &&
            !empty($range[0]->max)) {
            $prices["min"] = $range[0]->min;
            $prices["max"] = $range[0]->max;
        }

        return $prices;
    }

    protected function getMinPrice() {

    }

    protected function getSearchParams(){

        $this->publishedIds = $this->getPublishedPostIds();

        $priceRange = $this->getPriceRange();

        $arguments = array(
            'type'         => 'post',
            'child_of'     => 0,
            'parent'       => '',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => false,
            'hierarchical' => 1,
            'number'       => '',
            'taxonomy'     => 'category',
            'pad_counts'   => false
        );

        $selections = wp_get_nav_menu_items("selections");

        $arguments['taxonomy'] = 'concentration';
        $consists = get_categories($arguments);

        $arguments['post_type'] = 'brands';
        $brands = get_posts($arguments);

        $arguments['taxonomy'] = 'volume';
        $volumes = get_categories($arguments);

        $arguments['taxonomy'] = 'notes';
        $notes = get_categories($arguments);

        $arguments['taxonomy'] = 'spellings';
        $spellings = get_categories($arguments);

        $data = array(
            "brands" => $brands,
            "selections" => $selections,
            "consists" => $consists,
            "volumes" => $volumes,
            "notes" => $notes,
            "spellings" => $spellings,
            "priceKey" => "RUR",
            "volumeKey" => "мл.",
            "max" => $priceRange["max"],
            "min" => $priceRange["min"]
        );

        return $data;
    }

    protected function setData() {
        $filters = $this->getSearchParams();

//        print_r($filters);

        $this->data = Array(
            'filters' => $filters
        );
    }
}