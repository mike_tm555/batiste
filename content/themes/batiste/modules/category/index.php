<?php
namespace Modules;

class Category extends \Batiste\Core\Module
{
    protected function setData() {

        $category = get_category(get_query_var('cat'));
        $superCat = $this->getSuperCategory($category);
        $childes  = get_term_children($superCat->cat_ID, "category");
        $childes  = get_term_children($superCat->cat_ID, "category");

        print_r(get_categories());

        $this->data = Array(

        );
    }

    public function getSuperCategory($child) {
        $superCategory = $child;

        if($child->parent != 0) {
            $parent = get_category($child->parent);
            $superCategory = $this->getSuperCategory($parent);
        }

        return $superCategory;
    }
}