<?php

namespace Modules;


class Subscribe extends \Batiste\Core\Module
{
    protected function setData() {
        $logo = get_module("Logo", Array("type" => "white"));

        $this->data = array(
            "logo" => $logo
        );
    }
}