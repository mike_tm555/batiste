<div class="module-subscribe">
    <div class="app-fill">
        <div class="app-rel app-box app-center subscribe-block">
            <div class="app-rel app-box subscribe-section">
                {$logo}
            </div>
            <div class="app-rel app-box subscribe-section">
                <input type="email" class="app-rel app-box subscribe-input" placeholder="Введите Ваш email адрес"/>
                <button class="waves-effect waves-light btn app-rel app-box subscribe-button">Подписаться</button>
            </div>
        </div>
    </div>
</div>