<?php

namespace Modules;


class Selection extends \Batiste\Core\Module
{
    protected function setData() {
        $view = false;

        if(array_key_exists("view", $this->args)) {
            $view = $this->args["view"];
        }

        $this->data = Array(
          "view" => $view
        );
    }
}