<div class="app-rel app-box app-middle-center payment-module">
    <div class="app-rel app-box payment-title">
        Принимаем:
    </div>
    <div class="app-rel app-box payment-icons">
        <div class="app-rel app-box pay-icon pay-visa"></div>
        <div class="app-rel app-box pay-icon pay-master"></div>
        <div class="app-rel app-box pay-icon pay-web"></div>
        <div class="app-rel app-box pay-icon pay-yandex"></div>
        <div class="app-rel app-box pay-icon pay-qiv"></div>
    </div>
</div>