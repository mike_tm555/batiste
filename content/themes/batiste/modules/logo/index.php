<?php

namespace Modules;


class Logo extends \Batiste\Core\Module
{
    protected function setData() {
        $type = "black";
        if(array_key_exists("type", $this->args)) {
            $type = $this->args["type"];
        }

        $this->data = array(
            "type" => $type
        );
    }
}