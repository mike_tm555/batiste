<?php

namespace Modules;


class Popular extends \Batiste\Core\Module
{
    protected function setData() {
        $arguments = array(
            'post_type' => 'products',
            'tax_query' => array(
                array(
                    'taxonomy' => 'popularity',
                    'field' => "slug",
                    'terms' => 'popular',
                )
            )
        );

        $popularItems = Array();
        $popularPosts = get_posts($arguments);

        foreach($popularPosts as $post) {
           $popularItems[] = get_module('Item', Array('ID' => $post->ID));
        }

        $this->data = Array(
          'items' => $popularItems
        );
    }
}