<div class="module-popular">
    <div class="app-fill">
        <div class="app-rel app-box app-section">
            <h2 class="app-rel app-box app-center app-vector-title">
                <span>Самые популярные</span>
            </h2>

            <div class="app-rel app-box app-content">
                {foreach from=$items item=item}
                    <div class="app-rel app-box popular-item">
                        {$item}
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>