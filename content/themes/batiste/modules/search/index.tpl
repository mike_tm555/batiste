<div class="app-rel app-box search-module">
    <div class="app-rel app-box search-block">
        <div class="app-abs app-box search-icon">
            <i class="fa fa-search"></i>
        </div>
        <input type="search"
               class="app-rel app-box search-input"
               placeholder="Что Вы ищите?"/>
    </div>
    <div id="search-results"
         class="app-abs app-box search-results">
    </div>
</div>