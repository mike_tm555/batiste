<div class="module-recommend">
    <div class="app-fill">
        <div class="app-rel app-box app-section">
            <h2 class="app-rel app-box app-vector-title">
                <span>Batiste-Parfume Рекомендует</span>
            </h2>

            <div class="app-rel app-box app-content">
                {foreach from=$items item=item}
                    <div class="app-rel app-box recommend-item">
                        {$item}
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>