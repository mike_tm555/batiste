<?php

namespace Modules;


class Recommend extends \Batiste\Core\Module
{
    protected function setData() {
        $arguments = array(
            'post_type' => 'products',
            'tax_query' => array(
                array(
                    'taxonomy' => 'popularity',
                    'field' => "slug",
                    'terms' => 'recommended',
                )
            )
        );

        $recommendItems = Array();
        $recommendPosts = get_posts($arguments);

        foreach($recommendPosts as $post) {
            $recommendItems[] = get_module('Item', Array('ID' => $post->ID));
        }

        $this->data = Array(
            'items' => $recommendItems
        );
    }
}