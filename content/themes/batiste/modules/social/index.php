<?php

namespace Modules;


class Social extends \Batiste\Core\Module
{
    protected function setData(){
        $actionType = "share";

        if(array_key_exists("action", $this->args)) {
            $actionType = $this->args["action"];
        }

        $this->data = array(
            "action" => $actionType
        );
    }
}