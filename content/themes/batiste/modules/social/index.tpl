<div class="app-rel app-box app-middle-center social-module">
        {if $action eq "navigate"}
            <div class="app-rel app-box social-title">
                Присоединитесь!
            </div>
            <div class="app-rel app-box icons-block">
                <div class="app-rel app-box app-circle social-icon social-v">
                    <i class="fa fa-vk"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-o">
                    <i class="fa fa-odnoklassniki"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-f">
                    <i class="fa fa-facebook-f"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-i">
                    <i class="fa fa-instagram"></i>
                </div>
            </div>
        {else}
            <div class="app-rel app-box icons-block">
                <div class="app-rel app-box app-circle social-icon social-v">
                    <i class="fa fa-vk"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-o">
                    <i class="fa fa-odnoklassniki"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-f">
                    <i class="fa fa-facebook-f"></i>
                </div>
                <div class="app-rel app-box app-circle social-icon social-i">
                    <i class="fa fa-instagram"></i>
                </div>
            </div>
        {/if}
</div>