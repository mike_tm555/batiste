<div class="app-rel app-box module-item">
    <div class="app-rel app-box item-image">
        <a href="{$item.link}"
           class="app-rel app-box product-image"
           style="background-image: url('{$item.thumb}')">
        </a>
    </div>
    <div class="app-rel app-box item-name">
        <a href="{$item.link}">
            {$item.data->post_title}
        </a>
    </div>
    <div class="app-rel app-box item-price">
        <span class="price-value">
            {$item.priceMin|number_format:2:".":" "}
        </span>
        <span class="price-key">
            {$item.priceKey}
        </span>
    </div>
    <div class="app-rel app-box item-tools">
        <div class="app-rel app-box app-center item-tools-block">
            <div class="app-rel app-box item-tool">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="app-rel app-box item-tool">
                <i class="fa fa-heart"></i>
            </div>
        </div>
    </div>
</div>