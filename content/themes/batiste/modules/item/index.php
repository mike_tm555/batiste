<?php
namespace Modules;


class Item extends \Batiste\Core\Module
{
    public function getItemData($postId) {
        $itemIfo = Array();

        //images
        $imagesArray = get_field("product_images", $postId);

        //volumes and prices
        $pricesArray  = Array();
        $volumePrices = get_field("volume", $postId);

        if(is_array($volumePrices) && count($volumePrices) > 0) {
            foreach ($volumePrices as $key => $volumeItem) {
                $pricesArray[$key] = $volumeItem['price'];
            }
        }

        $minPrice = min($pricesArray);

        //spelling notes
        $notesObject  = get_field("notes", $postId);

        //item category
        $categoryObject = get_the_category($postId);

        foreach($categoryObject as $key => $category) {
            $categoryObject[$key]->link = get_category_link($category->cat_ID);
        }

        //spelling groups
        $spellingsObject = get_field("spellings", $postId);

        //consists groups
        $consistsObject = get_field("concentration", $postId);

        //selection groups
        $selectionObject = get_field("selection", $postId);

        if(is_array($selectionObject) && count($selectionObject)) {
            foreach($selectionObject as $key => $selection) {
                $selectionObject[$key]->link = get_permalink($selection->ID);
            }
        }

        //brand info
        $brandObject = get_field("brand", $postId);
        $brandImages = get_field("gallery", $brandObject->ID);

        $brandObject->images = $brandImages;
        $brandObject->icon   = $brandImages[0]["sizes"]["medium"];

        $brandObject->link = get_permalink($brandObject->ID);

        $itemIfo['data']      = get_post($postId);
        $itemIfo['link']      = get_permalink($postId);
        $itemIfo['thumb']     = $imagesArray[0]["sizes"]["medium"];
        $itemIfo['icon']      = $imagesArray[0]["sizes"]["medium"];
        $itemIfo['images']    = $imagesArray;
        $itemIfo['prices']    = $volumePrices;
        $itemIfo['priceMin']  = $minPrice;
        $itemIfo['priceKey']  = "RUR";
        $itemIfo['measure']   = "мл.";
        $itemIfo['brand']     = $brandObject;
        $itemIfo['notes']     = $notesObject;
        $itemIfo['spellings'] = $spellingsObject;
        $itemIfo['consist']   = $consistsObject;
        $itemIfo['selection'] = $selectionObject;
        $itemIfo['category']  = $categoryObject;

        return $itemIfo;
    }

    protected function setData() {
        $itemId   = $this->args["ID"];
        $itemInfo = $this->getItemData($itemId);

        $this->data = Array(
            'item' => $itemInfo
        );
    }
}