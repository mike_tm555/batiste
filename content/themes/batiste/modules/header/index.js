PubSub.subscribe('document.ready', function() {
    $("#side-toggle").sideNav({
        edge: 'left',
        menuWidth: 220,
        closeOnClick: true
    });
});