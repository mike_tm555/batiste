<div class="header-module app-fix app-box">
    <div class="app-fill">
        <div class="app-rel app-box header-tools">

            {if $mobile}
            <div data-activates="side-nav"
                 id="side-toggle"
                 class="app-left app-rel app-box app-full-h side-toggle">
                <div class="app-rel app-box app-tsn03 header-tool-item">
                    <span class="header-tool-icon">
                        <i class="fa fa-bars"></i>
                    </span>
                </div>
            </div>
            {/if}

            <div class="app-left app-rel app-box app-full-h">
                <div class="app-rel app-box app-tsn03 header-tool-item">
                    <span class="header-tool-icon">
                        <i class="fa fa-bank"></i>
                    </span>
                    <span class="header-tool-name">Мой магазин</span>
                </div>
            </div>

            <div class="app-right app-rel app-box app-full-h">
                <div class="app-rel app-box app-tsn03 header-tool-item">
                    <span class="header-tool-icon">
                        <i class="fa fa-user"></i>
                    </span>
                    <span class="header-tool-name">Войти</span>
                </div>

                <div class="app-rel app-box app-tsn03 header-tool-item">
                    <span class="header-tool-icon">
                        <i class="fa fa-heart"></i>
                    </span>
                    <span class="header-tool-name">Желанные</span>
                    <span class="header-tool-count">2</span>
                </div>

                <div class="app-rel app-box app-tsn03 header-tool-item">
                    <span class="header-tool-icon">
                        <i class="fa fa-shopping-bag"></i>
                    </span>
                    <span class="header-tool-name">Корзина</span>
                    <span class="header-tool-count">1</span>
                </div>
            </div>
        </div>
    </div>
</div>