<?php
namespace Modules;

class Header extends \Batiste\Core\Module
{
    protected function setData() {
        $this->data = array(
            "mobile" => wp_is_mobile()
        );
    }
}