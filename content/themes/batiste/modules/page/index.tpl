<div class="module-page">
    <div class="app-fill">
        <div class="app-rel app-box mobile-page-menu">
            <div class="drop-button" data-activates="drop-page-menu">
                <span class="drop-name">{$parent->post_title}</span>
                <span class="drop-icon"><i class="fa fa-bars"></i></span>
            </div>

            <ul id="drop-page-menu" class="dropdown-content">
                {foreach from=$childes item=child}
                    <li>
                        <a href="{$child->link}"
                           class="{($child->ID == $active) ? "active" : ""} app-rel app-box drop-page-item">
                            {$child->post_title}
                        </a>
                    </li>
                {/foreach}
            </ul>
        </div>

        <div class="app-rel app-box page-wrapper">
            <div class="app-abs app-box page-menu">
                {foreach from=$childes item=child}
                    <a href="{$child->link}"
                       class="{($child->ID == $active) ? "active" : ""} app-rel app-box page-menu-item">
                        {$child->post_title}
                    </a>
                {/foreach}
            </div>

            <div class="app-rel app-box page-content">
                <div class="app-rel app-box page-content-inner">
                    <div class="app-rel app-box page-title">
                        <h1 class="app-rel app-box app-center app-vector-title">
                            <span>{$title}</span>
                        </h1>
                    </div>
                    <div class="app-rel app-box page-content-text">
                        {$content}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>