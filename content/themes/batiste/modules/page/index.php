<?php

namespace Modules;


class Page extends \Batiste\Core\Module
{
    protected function setData(){
        global $post;

        $parentId = ($post->post_parent == 0) ? $post->ID : $post->post_parent;

        $args = array(
            'post_parent' => $parentId,
            'post_type'   => 'any',
            'numberposts' => -1,
            'post_status' => 'any'
        );

        $parent  = get_post($parentId);
        $childes = get_children($args);

        foreach($childes as $child) {
            $child->link = get_permalink($child->ID);
        }

        $active  = $post->ID;
        $content = $post->post_content;
        $content = apply_filters('the_content', $content);

        $this->data = array(
            "active" => $active,
            "parent" => $parent,
            "childes" => $childes,
            "content" => $content,
            "title" => $post->post_title
        );
    }
}