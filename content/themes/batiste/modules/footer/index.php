<?php

namespace Modules;


class Footer extends \Batiste\Core\Module
{
    protected function setData() {
        $navServices  = get_module("Navigation", Array('name' => 'services'));
        $navCompany   = get_module("Navigation", Array('name' => 'company'));
        $navCatalog   = get_module("Navigation", Array('name' => 'catalog'));

        $socialIcons  = get_module("Social", Array('action' => 'navigate'));
        $paymentIcons = get_module("Payment", Array('action' => 'navigate'));


        $this->data = array(
            "navServices" => $navServices,
            "navCompany" => $navCompany,
            "navCatalog" => $navCatalog,
            "social" => $socialIcons,
            "payment" => $paymentIcons
        );
    }
}