<div class="footer-module">
    <div class="app-fill">
        <div class="app-rel app-box footer-links">
            <div class="app-rel app-box navigation-list">

                {$navServices}

                {$navCompany}

                {$navCatalog}

                <div class="app-rel app-box navigation-block">
                    <h4 class="app-rel app-box navigation-parent">
                        Kонтактная информация
                    </h4>
                    <div class="app-rel app-box contact-item">
                        <div class="app-rel app-box contact-title">
                            Мы работаем
                        </div>
                        <div class="app-rel app-box contact-info">
                            с 9:00 до 22:00 в будни
                        </div>
                        <div class="app-rel app-box contact-info">
                            с 9:00 до 21:00 в выходные
                        </div>
                    </div>
                    <div class="app-rel app-box contact-item">
                        <div class="app-rel app-box contact-title">
                            Email-адрес
                        </div>
                        <div class="app-rel app-box contact-info">
                            <a class="app-rel app-box contact-email" href="info@batiste-parfume.ru">
                                info@batiste-parfume.ru
                            </a>
                        </div>
                    </div>
                    <div class="app-rel app-box contact-item">
                        <div class="app-rel app-box contact-title">
                            Оформить заказ на сайте можно круглосуточно
                        </div>
                        <div class="app-rel app-box contact-info">
                            <span class="app-rel app-box contact-phone">
                                +7 (495) 374-78-74
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="app-rel app-box footer-bottom">
        <div class="app-rel app-box footer-section">
            <div class="app-fill">
                <div class="app-rel app-box footer-section-block">
                    {$payment}
                </div>
                <div class="app-rel app-box footer-section-block">
                    {$social}
                </div>
            </div>
        </div>
        <div class="app-rel app-box footer-section">
            <div class="app-fill">
                <div class="app-rel app-box footer-section-block">
                    <div class="app-rel app-box app-center footer-section-title">
                        © Batiste-Parfume.ru, 2014–2016
                    </div>
                </div>
                <div class="app-rel app-box footer-section-block">
                    <div class="app-rel app-box app-center footer-section-title">
                        Все права защишены "ООО Batiste".
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>