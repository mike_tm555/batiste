<?php

namespace Modules;


class Navigation extends \Batiste\Core\Module
{
    protected function setData() {
        $menu = wp_get_nav_menu_items($this->args["name"]);

        $newMenu = Array();

        if($this->args["name"] == "catalog") {

        }

        foreach($menu as $key => $value) {
            if($value->menu_item_parent == '0') {
                $arrayKey = $value->ID;
                $newMenu[$arrayKey]['parent'] = $value;
            } else {
                $arrayKey = $value->menu_item_parent;
                $newMenu[$arrayKey]["child"][$key] = $value;
            }
        }

        $this->data = array(
            "menu" => $newMenu
        );
    }
}