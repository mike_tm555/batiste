{foreach from=$menu item=menuItem}
    <div class="app-rel app-box navigation-block">
        <h4 class="app-rel app-box navigation-parent">
            {$menuItem.parent->title}
        </h4>
        <div class="app-rel app-box navigation-childes">
            {foreach from=$menuItem.child item=childItem}
                <div class="app-rel app-box app-tsn03 navigation-child">
                    <a href="{$childItem->url}">
                        {$childItem->title}
                    </a>
                </div>
            {/foreach}
        </div>
    </div>
{/foreach}