<?php
namespace Modules;

class Products extends \Batiste\Core\Module
{
    protected function setData() {

        $category = get_module("Category");

        $logo = get_module("Logo", Array("type" => "white"));

        $this->data = Array(
            "mobile" => wp_is_mobile(),
            "logo" => $logo,
            "category" => $category,
            "filter" => get_module("Filter")
        );
    }
}