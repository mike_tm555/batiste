<div class="module-products">
    <div class="app-fill">
        {if !$mobile}
            <div class="app-rel app-box products-block-app">
                <div class="app-rel app-box products-filter-app">
                    <div>{$category}</div>
                    <div>{$filter}</div>
                </div>

                <div class="app-rel app-box products-container">
                    <div class="app-rel app-box products-wrapper">
                    </div>
                </div>
            </div>
        {else}
            <div id="products-filter-nav" class="side-nav">
                <div class="app-rel side-logo">
                    {$logo}
                </div>
                <div class="app-rel app-box products-filter-mobile">
                    {$filter}
                </div>
            </div>

            <div class="app-rel app-box products-block-mobile">
                <div class="app-rel app-box search-tools">
                    <div class="app-rel app-box search-tool-item"
                         id="filter-toggle"
                         data-activates="products-filter-nav">
                        <span class="search-tool-icon"><i class="fa fa-cog"></i></span>
                        <span class="search-tool-name">Фильтры</span>
                    </div>
                    <div class="app-rel app-box search-tool-item"
                         id="category-toggle"
                         data-activates="products-category-nav">
                        <span class="search-tool-icon"><i class="fa fa-list-ul"></i></span>
                        <span class="search-tool-name">Kaтегории</span>
                    </div>
                </div>

                <div class="app-rel app-box products-container">
                    <div class="app-rel app-box products-wrapper">
                    </div>
                </div>
            </div>
        {/if}
    </div>
</div>