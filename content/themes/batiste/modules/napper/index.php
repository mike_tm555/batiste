<?php

namespace Modules;


class Napper extends \Batiste\Core\Module
{
    protected function setData() {
        $search = get_module("Search");
        $logo = get_module("Logo");
        $phone = get_module("Phone");
        $selection = get_module("Selection");

        $this->data = Array(
            "search" => $search,
            "logo" => $logo,
            "phone" => $phone,
            "selection" => $selection
        );
    }
}