<div class="napper-module">
    <div class="app-fill">
        <div class="app-rel app-box app-napper">
            <div class="app-rel app-box app-napper-section">
                {$logo}
            </div>
            <div class="app-rel app-box app-napper-section">
                {$phone}
            </div>
            <div class="app-rel app-box app-napper-section">
                {$selection}
            </div>
            <div class="app-rel app-box app-napper-section">
                {$search}
            </div>
        </div>
    </div>
</div>