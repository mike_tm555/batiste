{if !$mobile}
    <div class="menu-module">
        <div class="app-fill">
            <div class="app-rel app-box app-menu">
                {$menu}
            </div>
        </div>
    </div>
{else}
    <div id="side-nav" class="side-nav">
        <div class="app-rel app-box side-logo">
            {$logo}
        </div>
        <div class="app-rel app-box side-menu">
            {$menu}
        </div>
    </div>
{/if}