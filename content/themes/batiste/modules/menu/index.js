PubSub.subscribe('document.ready', function() {
    $("#mobile-menu > ul > li > .menu-icon").click(function (e) {
        if($(this).next(".sub-menu").hasClass('selected')) {
            $("#mobile-menu .selected ul").slideUp(100);
            $("#mobile-menu .selected").removeClass("selected");
        } else {
            $("#mobile-menu .selected ul").slideUp(100);
            $("#mobile-menu .selected").removeClass("selected");
            if ($(this).next(".sub-menu").length) {
                $(this).next(".sub-menu").addClass("selected");
                $(this).next(".sub-menu").children().slideDown(200);
            }
        }
        e.stopPropagation();
    });
});