<?php
namespace Modules;

class Menu extends \Batiste\Core\Module
{
    protected function setData() {
        $mobileLogo = get_module("Logo", Array("type" => "white"));
        $mobileMenu = wp_is_mobile();

        if($mobileMenu) {
            $catOpenIcon  = "<span class='menu-icon'>";
            $catOpenIcon .= "<i class='fa fa-angle-down'></i>";
            $catOpenIcon .= "</span>";

            $categories = wp_nav_menu(
                array(
                    "menu" => "category",
                    "echo" => false,
                    "after" => $catOpenIcon,
                    "container_id" => "mobile-menu"
                )
            );
        } else {
            $categories = wp_nav_menu(
                array(
                    "menu" => "category",
                    "echo" => false,
                    "container_id" => "app-menu"
                )
            );
        }

        $this->data = array(
            "menu"   => $categories,
            "mobile" => $mobileMenu,
            "logo"   => $mobileLogo
        );
    }
}