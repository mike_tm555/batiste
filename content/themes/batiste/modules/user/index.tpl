<div class="user-module">
    <div class="app-rel app-box user-field">
        [reg-email id="email"]
    </div>
    <div class="app-rel app-box user-field">
        [reg-password id="password"]
    </div>
    <div class="app-rel app-box user-field">
        [reg-first-name id="first-name"]
    </div>
    <div class="app-rel app-box user-field">
        [reg-last-name id="last-name"]
    </div>
    <div class="app-rel app-box user-field">
        [reg-submit class="btn" value="Create Account"]
    </div>
</div>