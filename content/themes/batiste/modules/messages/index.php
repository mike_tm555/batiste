<?php

namespace Modules;


class Page extends \Batiste\Core\Module
{
    protected function setData(){
        global $post;
        $content = $post->post_content;
        $content = apply_filters('the_content', $content);
        $this->data = array(
            "content" => $content,
            "title" => $post->post_title,
        );
    }
}