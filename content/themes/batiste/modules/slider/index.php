<?php
namespace Modules;

class Slider extends \Batiste\Core\Module
{
    protected function getItemData($data, $caption = false) {
        $sliderItem = Array(
            "image" => (array_key_exists("image", $data)) ? $data["image"] : false,
            "title" => (array_key_exists("title", $data)) ? $data["title"] : false,
            "text" => (array_key_exists("text", $data)) ? $data["text"] : false,
            "link" => (array_key_exists("link", $data)) ? $data["link"] : false,
            "captionClass" => $caption
        );

        return $sliderItem;
    }

    protected function setData() {

        if(!empty($this->args) && !empty($this->args['data'])) {
            $sliderItems = array();
            foreach ($this->args['data'] as $item){
                $sliderItems[] = $this->getItemData($item, "right");
            }
            $this->data = array("slider" => $sliderItems);
            return;
        }

        $sliderType = (!empty($this->args) && !empty($this->args['type'])) ? $this->args['type'] : "main_slider";

        $args = array(
            'post_type' => 'slider',
            'tax_query' => array(
                array(
                    'taxonomy' => 'slider',
                    'field' => "slug",
                    'terms' => $sliderType,
                )
            )
        );

        $postItems = get_posts($args);

        $sliderItems = array();

        foreach($postItems as $postItem) {
            $thumb = get_field("slider_images", $postItem->ID);
            $image = $thumb[0]["sizes"]["large"];
            $name  = $postItem->post_title;
            $link  = get_field("slider_destination", $postItem->ID);
            $text  = $postItem->post_excerpt;

            $sliderItems[] = array(
                "image" => $image,
                "title" => $name,
                "text" => $text,
                "link" => $link,
                "captionClass" => "right"
            );
        }

        $this->data = array(
            'slider' => $sliderItems
        );
    }
}