<div class="module-slider">
    <div class="slider fullscreen">
        <span class="slide-button slide-prev">
            <i class="fa fa-angle-left"></i>
        </span>
        <span class="slide-button slide-next">
            <i class="fa fa-angle-right"></i>
        </span>
        <ul class="slides">
            {foreach from=$slider item=sliderItem}
                <li>
                    <a href="{$sliderItem.link}">
                        <img src="{$sliderItem.image}">
                        <div class="caption {$sliderItem.captionClass}-align">
                            {if $sliderItem.text}
                                <h3>{$sliderItem.text}</h3>
                            {/if}
                        </div>
                    </a>
                </li>
            {/foreach}
        </ul>
    </div>
</div>