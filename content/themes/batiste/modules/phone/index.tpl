<div class="app-rel app-box app-phone">
    <div class="app-rel app-box phone-block">
        <div class="app-rel app-box phone-icon">
            <i class="fa fa-phone"></i>
        </div>
        <div class="app-rel app-box phone-number">
            <span class="p-code">+7 (916)</span>
            <span class="p-number">997-5900</span>
        </div>
    </div>
</div>