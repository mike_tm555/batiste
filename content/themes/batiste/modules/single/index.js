PubSub.subscribe('document.ready', function() {
    var itemSlider = $("#single-item-slider");
    var itemThumbs = $("#single-item-thumbs");

    itemSlider.slider({
        Indicators: false,
        full_width: true
    });

    $('.single-item-gallery').hover(function(){
        itemSlider.slider('pause');
    }, function(){
        itemSlider.slider('start');
    });

    $('.item-slide-next').click(function(){
        itemSlider.slider('next');
    });

    $('.item-slide-prev').click(function() {
        itemSlider.slider('prev');
    });
});