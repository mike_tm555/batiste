<div class="module-single">
    <div class="app-fill">
        <div class="app-rel app-box single-item-block">
            <div class="app-rel app-box single-item-title">
                <h2 class="app-rel app-box app-center app-vector-title">
                    <span>{$single.data->post_title}</span>
                </h2>
            </div>

            <div class="app-rel app-box single-item-header">
                <div class="app-rel app-box single-header-child">
                    <a href="{$single.brand->link}"
                       style="background-image: url({$single.brand->icon})"
                       class="app-rel app-box single-header-tool item-brand-icon">
                    </a>
                </div>
                <div class="app-rel app-box single-header-child">
                    <a href="{$single.selection[0]->link}"
                       class="app-rel app-box single-header-tool item-selection-type">
                        {$single.selection[0]->post_title}
                    </a>
                </div>
                <div class="app-rel app-box single-header-child">
                    <div class="app-rel app-box single-header-tool item-to-bookmark">
                        <span class="icon"><i class="fa fa-heart"></i></span>
                        <span class="value">В избранное</span>
                    </div>
                </div>
            </div>

            <div class="app-rel app-box single-item-gallery">
                <div class="slider">
                    <div class="slide-button item-slide-prev">
                        <i class="fa fa-angle-left"></i>
                    </div>
                    <div class="slide-button item-slide-next">
                        <i class="fa fa-angle-right"></i>
                    </div>
                    <ul class="app-rel app-box slides" id="single-item-slider">
                        {foreach from=$single.images item=image}
                            <li>
                                <div class="app-rel app-box slider-item"
                                     style="background-image: url({$image.sizes.medium})">
                                </div>
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>

            <div class="app-rel app-box single-item-prices">
                {foreach from=$single.prices item=price}
                    <div class="app-rel app-box price-block">
                        <div class="app-rel app-box price-item-half">
                            <div class="app-rel app-box price-item price-image"
                                 style="background-image: url({$single.icon})">
                            </div>
                            <div class="app-rel app-box price-item price-name">
                                <span class="value">{$single.data->post_title}</span>
                                <span class="value">{$price.milli->name}</span>
                                <span class="value">{$single.measure}</span>
                            </div>
                        </div>

                        <div class="app-rel app-box price-item-half">
                            <div class="app-rel app-box price-item price-value">
                                <span class="value">{$price.price|number_format:2:".":" "}</span>
                                <span class="key">{$single.priceKey}</span>
                            </div>
                            <div class="app-rel app-box price-item cart-button">
                                <div class="app-rel app-middle waves-effect waves-light btn">
                                    В корзину
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>

            <div class="app-rel app-box single-item-title">
                <h2 class="app-rel app-box app-center app-vector-title">
                    <span>Информация</span>
                </h2>
            </div>

            <div class="app-rel app-box single-item-info">
                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        Бранд
                    </div>
                    <div class="app-rel app-box info-value">
                        <a href="{$single.brand->link}" class="info-value-item">
                            {$single.brand->post_title}
                        </a>
                    </div>
                </div>

                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        Артикул
                    </div>
                    <div class="app-rel app-box info-value">
                        <span class="info-value-item">
                            {$single.data->ID}
                        </span>
                    </div>
                </div>

                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        Концентрация
                    </div>
                    <div class="app-rel app-box info-value">
                        <span class="info-value-item">
                            {$single.consist->name}
                        </span>
                    </div>
                </div>

                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        Категория
                    </div>
                    <div class="app-rel app-box info-value">
                        {foreach from=$single.category item=catItem}
                            <a href="{$catItem->link}" class="info-value-item">
                                {$catItem->name}
                            </a>
                        {/foreach}
                    </div>
                </div>
            </div>

            <div class="app-rel app-box separator"></div>

            <div class="app-rel app-box single-item-characters">
                <div class="app-rel app-box item-tabs">
                    <ul class="app-rel app-box app-center tabs">
                        <li class="tab col s3"><a href="#chars">Характеристики</a></li>
                        <li class="tab col s3"><a href="#desc">Описание</a></li>
                    </ul>
                </div>
                <div class="app-rel app-box tab-content">
                    <div id="chars" class="app-rel app-box tab-info">

                        <div class="app-rel app-box character-item">
                            <div class="character-title">
                                Группы ароматов
                            </div>

                            <div class="app-rel app-box row character-content">
                                {foreach from=$single.spellings item=spelling}
                                    <div class="character-child col l3 m4 s12">
                                        <i class="fa fa-check"></i>
                                        <span>{$spelling->name}</span>
                                    </div>
                                {/foreach}
                            </div>
                        </div>

                        <div class="app-rel app-box character-item">
                            <div class="character-title">
                                Ноты ароматов
                            </div>

                            <div class="app-rel app-box row character-content">
                                {foreach from=$single.notes item=note}
                                    <div class="character-child col l3 m4 s12">
                                        <i class="fa fa-check"></i>
                                        <span>{$note->name}</span>
                                    </div>
                                {/foreach}
                            </div>
                        </div>

                    </div>
                    <div id="desc" class="app-rel app-box tab-info">
                        {$single.data->post_content}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>