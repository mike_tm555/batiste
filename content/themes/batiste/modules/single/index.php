<?php
namespace Modules;

class Single extends \Batiste\Core\Module
{
    protected function setData() {
        global $post;

        $singleId   = $post->ID;
        $itemClass  = new Item();
        $singleInfo = $itemClass->getItemData($singleId);

        $this->data = Array(
            'single' => $singleInfo
        );
    }
}