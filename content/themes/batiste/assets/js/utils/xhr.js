function xhr(data, successCallback, failCallback){

    data = Object.keys(data).map(function (k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
    }).join('&');

    $.post('/wordpress/wp-admin/admin-ajax.php', data, function( response ) {
        if(response) {
            var req_data = response;
            try {
                req_data = JSON.parse(response);
            } catch(e) {

            }
            successCallback( req_data );
        } else {
            failCallback( response );
        }

    });
}

module.exports = xhr;
