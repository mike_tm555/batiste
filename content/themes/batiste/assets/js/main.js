(function ($, PubSub, Materialize, noUiSlider, stickyScroll) {

    require('utils/fb_widget');

    $(document).ready((e) => {
        PubSub.publish('document.ready',e);
        $('ul.tabs').tabs();
        $('.parallax').parallax();
    });

    $(window).load((e) => {
        PubSub.publish('window.load',e);
    });

    $(window).resize((e) => {
        PubSub.publish('window.resize',e);
    });

    $(window).scroll((e) => {
        PubSub.publish('window.scroll',e);
    });

    $.fn.serializeFormObject = function() {

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value)  {
            base[key] = value;
            return base;
        };

        this.push_counter = function(key) {
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function (){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };

    window.mq = false;

    window.resetMQ = function () {
        var w = $(window).get(0).innerWidth;
        var oldMq = window.mq;
        if (w < 768) {
            window.mq = 'mobile';
        } else if (w >= 768 && w < 1024) {
            window.mq = 'tabletportrait';
        } else if (w >= 1024 && w < 1080) {
            window.mq = 'tabletlandscape';
        }else if (w >= 1080 && w < 1280) {
            window.mq = 'nonedesktop';
        } else {
            window.mq = 'desktop';
        }
        if(oldMq != false && oldMq != window.mq){
            PubSub.publish('layoutChanged', window.mq);
        }
    };

    function is_mob() {
        if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ){
            return true;
        }
        return false;
    }

    if(!is_mob()){
        $(window).on('resize', resetMQ);
    }else {
        $(window).on("orientationchange", resetMQ);
    }
    resetMQ()


}( jQuery, PubSub, Materialize, noUiSlider, stickyScroll));import 'modules/account/index.js';
import 'modules/addresses/index.js';
import 'modules/cart/index.js';
import 'modules/comments/index.js';
import 'modules/contact/index.js';
import 'modules/filter/index.js';
import 'modules/flow/index.js';
import 'modules/follow/index.js';
import 'modules/footer/index.js';
import 'modules/header/index.js';
import 'modules/item/index.js';
import 'modules/likes/index.js';
import 'modules/login/index.js';
import 'modules/logo/index.js';
import 'modules/menu/index.js';
import 'modules/messages/index.js';
import 'modules/napper/index.js';
import 'modules/navigation/index.js';
import 'modules/news/index.js';
import 'modules/orders/index.js';
import 'modules/page/index.js';
import 'modules/payment/index.js';
import 'modules/phone/index.js';
import 'modules/popular/index.js';
import 'modules/products/index.js';
import 'modules/recommend/index.js';
import 'modules/search/index.js';
import 'modules/selection/index.js';
import 'modules/sideBar/index.js';
import 'modules/single/index.js';
import 'modules/siteMap/index.js';
import 'modules/slider/index.js';
import 'modules/social/index.js';
import 'modules/sort/index.js';
import 'modules/subscribe/index.js';
import 'modules/user/index.js';
import 'modules/whishes/index.js';
