<?php
add_action( 'wp_ajax_nopriv_follow', 'titus_follow' );
add_action( 'wp_ajax_follow', 'titus_follow' );

function titus_follow(){
    $follow = new \Modules\Follow();
    $follow->follow();
    exit();
}

add_action( 'wp_ajax_nopriv_testimonials', 'titus_testimonials' );
add_action( 'wp_ajax_testimonials', 'titus_testimonials' );

function titus_testimonials(){
    $follow = new \Modules\Testimonials();
    $follow->add();
    exit();
}

add_action( 'wp_ajax_nopriv_contact_us', 'titus_contact_us' );
add_action( 'wp_ajax_contact_us', 'titus_contact_us' );

function titus_contact_us(){
    $follow = new \Modules\ContactUsForm();
    $follow->add();
    exit();
}


add_action( 'wp_ajax_nopriv_search_items', 'search_items' );
add_action( 'wp_ajax_search_items', 'search_items' );

function search_items(){
    $search = new \Modules\FullSearch();
    die(json_encode($search->getSearchItemsAjax()));
    exit();
}