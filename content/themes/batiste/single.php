<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 8/14/16
 * Time: 5:04 AM
 */
global $post;
get_header();
load_module('Header');
load_module('Napper');
load_module('Menu');
load_module('Flow');
load_module('Single');
load_module('Subscribe');
load_module('Footer');
get_footer();