<?php
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Batiste
 */
global $post;
get_header();
load_module('Header');
load_module('Napper');
load_module('Menu');
load_module('Flow');
load_module('Page');
load_module('Subscribe');
load_module('Footer');
get_footer();
